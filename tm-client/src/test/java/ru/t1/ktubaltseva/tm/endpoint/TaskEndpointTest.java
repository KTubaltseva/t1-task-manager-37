package ru.t1.ktubaltseva.tm.endpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.ktubaltseva.tm.api.endpoint.IProjectEndpoint;
import ru.t1.ktubaltseva.tm.api.endpoint.ITaskEndpoint;
import ru.t1.ktubaltseva.tm.api.endpoint.IUserEndpoint;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.dto.request.project.ProjectClearRequest;
import ru.t1.ktubaltseva.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.ktubaltseva.tm.dto.request.task.*;
import ru.t1.ktubaltseva.tm.dto.request.user.UserLoginRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserLogoutRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserRegistryRequest;
import ru.t1.ktubaltseva.tm.dto.request.user.UserRemoveRequest;
import ru.t1.ktubaltseva.tm.dto.response.project.ProjectCreateResponse;
import ru.t1.ktubaltseva.tm.dto.response.task.*;
import ru.t1.ktubaltseva.tm.dto.response.user.UserLoginResponse;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.marker.IntegrationCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;

import java.security.NoSuchAlgorithmException;

import static ru.t1.ktubaltseva.tm.constant.TaskTestData.*;

@Category(IntegrationCategory.class)
public final class TaskEndpointTest {

    @NotNull
    private final static ILoggerService loggerService = new LoggerService();

    @NotNull
    private final static IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private final static IUserEndpoint userEndpoint = IUserEndpoint.newInstance(propertyService);

    @NotNull
    private final static ITaskEndpoint endpoint = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private final static IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @Nullable
    private static Task userTask;

    @Nullable
    private static String userTaskId;

    @Nullable
    private static Project userProject;

    @Nullable
    private static String userProjectId;

    @BeforeClass
    public static void before() throws AbstractException, NoSuchAlgorithmException, JsonProcessingException {
        @NotNull final UserLoginRequest loginAdminRequest = new UserLoginRequest(ADMIN_LOGIN, ADMIN_PASSWORD);
        @NotNull final UserLoginResponse loginAdminResponse = authEndpoint.login(loginAdminRequest);
        adminToken = loginAdminResponse.getToken();

        @NotNull final UserRegistryRequest userRegistryRequest = new UserRegistryRequest(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        userEndpoint.registry(userRegistryRequest);

        @NotNull final UserLoginRequest loginUserRequest = new UserLoginRequest(USER_LOGIN, USER_PASSWORD);
        @NotNull final UserLoginResponse loginUserResponse = authEndpoint.login(loginUserRequest);
        userToken = loginUserResponse.getToken();

        @NotNull final ProjectCreateRequest projectCreateRequest = new ProjectCreateRequest(userToken, PROJECT_NAME, PROJECT_DESC);
        @NotNull final ProjectCreateResponse projectCreateResponse = projectEndpoint.createProject(projectCreateRequest);
        userProject = projectCreateResponse.getProject();
        userProjectId = userProject.getId();

        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken, TASK_NAME, TASK_DESC);
        @NotNull final TaskCreateResponse taskCreateResponse = endpoint.createTask(taskCreateRequest);
        userTask = taskCreateResponse.getTask();
        userTaskId = userTask.getId();
    }

    @AfterClass
    public static void after() throws AbstractException {
        endpoint.clearTasks(new TaskClearRequest(userToken));
        userTask = null;
        userTaskId = null;

        projectEndpoint.clearProjects(new ProjectClearRequest(userToken));
        userProject = null;
        userProjectId = null;

        @NotNull final UserLogoutRequest logoutUserRequest = new UserLogoutRequest(userToken);
        authEndpoint.logout(logoutUserRequest);
        userToken = null;

        @NotNull final UserRemoveRequest removeUserRequest = new UserRemoveRequest(adminToken, USER_LOGIN);
        userEndpoint.remove(removeUserRequest);

        @NotNull final UserLogoutRequest logoutAdminRequest = new UserLogoutRequest(adminToken);
        authEndpoint.logout(logoutAdminRequest);
        adminToken = null;
    }

    @Test
    public void getTaskById() throws AbstractException {
        @Nullable final TaskDisplayByIdResponse response = endpoint.getTaskById(new TaskDisplayByIdRequest(userToken, userTaskId));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());
    }

    @Test
    public void getAllTasks() throws AbstractException {
        @NotNull final TaskDisplayListResponse responseNoSort = endpoint.getAllTasks(new TaskDisplayListRequest(userToken));
        Assert.assertNotNull(responseNoSort);
        Assert.assertNotNull(responseNoSort.getTasks());

        @NotNull final TaskDisplayListResponse responseWithSort = endpoint.getAllTasks(new TaskDisplayListRequest(userToken, TASK_SORT));
        Assert.assertNotNull(responseWithSort);
        Assert.assertNotNull(responseWithSort.getTasks());
    }

    @Test
    public void clearTasks() throws AbstractException {
        @NotNull final TaskClearResponse response = endpoint.clearTasks(new TaskClearRequest(userToken));
        Assert.assertNotNull(response);

        @NotNull final TaskDisplayListResponse responseGetAll = endpoint.getAllTasks(new TaskDisplayListRequest(userToken, TASK_SORT));
        Assert.assertNull(responseGetAll.getTasks());

        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken, TASK_NAME, TASK_DESC);
        @NotNull final TaskCreateResponse taskCreateResponse = endpoint.createTask(taskCreateRequest);
        userTask = taskCreateResponse.getTask();
        userTaskId = userTask.getId();
    }

    @Test
    public void removeTaskById() throws AbstractException {
        @Nullable final TaskRemoveByIdResponse response = endpoint.removeTaskById(new TaskRemoveByIdRequest(userToken, userTaskId));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask());

        @NotNull final TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken, TASK_NAME, TASK_DESC);
        @NotNull final TaskCreateResponse taskCreateResponse = endpoint.createTask(taskCreateRequest);
        userTask = taskCreateResponse.getTask();
        userTaskId = userTask.getId();
    }

    @Test
    public void createTask() throws AbstractException {
        @NotNull final TaskCreateResponse response = endpoint.createTask(new TaskCreateRequest(userToken, TASK_NAME, TASK_DESC));
        Assert.assertNotNull(response);
        @Nullable final Task task = response.getTask();
        Assert.assertNotNull(task);
        Assert.assertNotNull(endpoint.getTaskById(new TaskDisplayByIdRequest(userToken, task.getId())));
    }

    @Test
    public void changeTaskStatusById() throws AbstractException {
        @NotNull final TaskChangeStatusByIdResponse response = endpoint.changeTaskStatusById(new TaskChangeStatusByIdRequest(userToken, userTaskId, TASK_STATUS));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getStatus());
        Assert.assertEquals(TASK_STATUS, response.getTask().getStatus());
    }

    @Test
    public void updateTaskById() throws AbstractException {
        @NotNull final TaskUpdateByIdResponse response = endpoint.updateTaskById(new TaskUpdateByIdRequest(userToken, userTaskId, TASK_NAME_NEW, TASK_DESC_NEW));
        Assert.assertNotNull(response);
        Assert.assertEquals(TASK_NAME_NEW, response.getTask().getName());
        Assert.assertEquals(TASK_DESC_NEW, response.getTask().getDescription());
    }

    @Test
    public void startTaskById() throws AbstractException {
        @NotNull final TaskStartByIdResponse response = endpoint.startTaskById(new TaskStartByIdRequest(userToken, userTaskId));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getStatus());
        Assert.assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
    }

    @Test
    public void completeTaskById() throws AbstractException {
        @NotNull final TaskCompleteByIdResponse response = endpoint.completeTaskById(new TaskCompleteByIdRequest(userToken, userTaskId));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getStatus());
        Assert.assertEquals(Status.COMPLETED, response.getTask().getStatus());
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        @NotNull final TaskBindToProjectResponse response = endpoint.bindTaskToProject(new TaskBindToProjectRequest(userToken, userTaskId, userProjectId));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTask().getProjectId());
        Assert.assertEquals(userProjectId, response.getTask().getProjectId());
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        @NotNull final TaskUnbindFromProjectResponse response = endpoint.unbindTaskFromProject(new TaskUnbindFromProjectRequest(userToken, userTaskId, userProjectId));
        Assert.assertNotNull(response);
        Assert.assertNull(response.getTask().getProjectId());
    }

    @Test
    public void getTasksByProjectId() throws AbstractException {
        endpoint.bindTaskToProject(new TaskBindToProjectRequest(userToken, userTaskId, userProjectId));
        @NotNull final TaskDisplayByProjectIdResponse response = endpoint.getTasksByProjectId(new TaskDisplayByProjectIdRequest(userToken, userProjectId));
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getTasks());
    }

}
