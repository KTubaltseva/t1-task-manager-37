package ru.t1.ktubaltseva.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.command.AbstractCommand;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.List;

public abstract class AbstractUserCommand extends AbstractCommand {

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void renderUsers(@Nullable final List<User> users) throws UserNotFoundException {
        if (users == null) throw new UserNotFoundException();
        int index = 1;
        for (@NotNull final User user : users) {
            System.out.println(index + ". " + user);
            System.out.println();
            index++;
        }
    }

    public void displayUser(@Nullable final User user) throws UserNotFoundException {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("ROLE: " + user.getRole());
    }

    protected void renderUsersFullInfo(@Nullable final List<User> users) throws UserNotFoundException {
        if (users == null) throw new UserNotFoundException();
        int index = 1;
        for (@NotNull final User user : users) {
            System.out.println(index + ".");
            displayUser(user);
            System.out.println();
            index++;
        }
    }

}