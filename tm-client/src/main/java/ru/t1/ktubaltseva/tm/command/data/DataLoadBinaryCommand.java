package ru.t1.ktubaltseva.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.dto.request.data.DataLoadBinaryRequest;
import ru.t1.ktubaltseva.tm.exception.AbstractException;

public class DataLoadBinaryCommand extends AbstractDataCommand {

    @NotNull
    private final String NAME = "data-load-binary";

    @NotNull
    private final String DESC = "Load data from binary file.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[LOAD BINARY DATA]");
        @NotNull final DataLoadBinaryRequest request = new DataLoadBinaryRequest(getToken());
        getDomainEndpoint().loadDataBinary(request);
    }

}
