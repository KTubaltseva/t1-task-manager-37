package ru.t1.ktubaltseva.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.task.TaskDisplayByIdRequest;
import ru.t1.ktubaltseva.tm.dto.response.task.TaskDisplayByIdResponse;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.util.TerminalUtil;

public final class TaskDisplayByIdCommand extends AbstractTaskCommand {

    @NotNull
    private final String NAME = "task-display-by-id";

    @NotNull
    private final String DESC = "Display task by Id.";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESC;
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[DISPLAY PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskDisplayByIdRequest request = new TaskDisplayByIdRequest(getToken(), id);
        @NotNull final TaskDisplayByIdResponse response = getTaskEndpoint().getTaskById(request);
        @Nullable final Task task = response.getTask();
        displayTask(task);
    }

}
