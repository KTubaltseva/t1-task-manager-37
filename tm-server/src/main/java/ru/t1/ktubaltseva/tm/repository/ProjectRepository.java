package ru.t1.ktubaltseva.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.field.StatusEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.StatusIncorrectException;
import ru.t1.ktubaltseva.tm.model.Project;

import java.sql.*;

public final class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @Getter
    private final String tableName = "tm_project";

    public ProjectRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Project add(@NotNull final Project project) throws SQLException {
        @NotNull final String sql = String.format(
                "insert into %s (id, created, user_id, name, description, status) " +
                        "values (?, ?, ?, ?, ?, ?)",
                tableName
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, project.getId());
            statement.setTimestamp(2, new Timestamp(project.getCreated().getTime()));
            statement.setString(3, project.getUserId());
            statement.setString(4, project.getName());
            statement.setString(5, project.getDescription());
            statement.setString(6, project.getStatus().toString());
            statement.executeUpdate();
        }
        return project;
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException {
        @NotNull final Project project = new Project(name, description);
        return add(userId, project);
    }

    @NotNull
    @Override
    public Project create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws SQLException {
        @NotNull final Project project = new Project(name);
        return add(userId, project);
    }

    @NotNull
    @Override
    protected Project fetch(@NotNull final ResultSet row) throws SQLException, StatusIncorrectException, StatusEmptyException {
        @NotNull final Project project = new Project();
        project.setId(row.getString("id"));
        project.setCreated(row.getTimestamp("created"));
        project.setUserId(row.getString("user_id"));
        project.setName(row.getString("name"));
        project.setDescription(row.getString("description"));
        project.setStatus(Status.toStatus(row.getString("status")));
        return project;
    }

    @NotNull
    @Override
    public Project update(@NotNull final Project project) throws SQLException {
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format(
                "update %s " +
                        "set created = ?, " +
                        "user_id = ?, " +
                        "name = ?, " +
                        "description = ?, " +
                        "status = ? " +
                        "where id = ?;",
                tableName
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(project.getCreated().getTime()));
            statement.setString(2, project.getUserId());
            statement.setString(3, project.getName());
            statement.setString(4, project.getDescription());
            statement.setString(5, project.getStatus().toString());
            statement.setString(6, project.getId());
            statement.executeUpdate();
        }
        return project;
    }

}
