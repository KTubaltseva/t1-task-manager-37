package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final IConnectionService connectionService;

    public AbstractService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    protected abstract R getRepository(@NotNull final Connection connection);

    protected void connectionRollback(@NotNull final Connection connection) throws SqlDataException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new SqlDataException(e);
        }
    }

    protected void connectionClose(@NotNull final Connection connection) throws SqlDataException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new SqlDataException(e);
        }
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) throws EntityNotFoundException, SqlDataException {
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModel = modelRepository.add(model);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultModel;
    }

    @NotNull
    @Override
    public Collection<M> add(@Nullable final Collection<M> models) throws SqlDataException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<M> resultModels;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModels = modelRepository.add(models);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultModels;
    }

    @NotNull
    @Override
    public Collection<M> set(@Nullable final Collection<M> models) throws AbstractException {
        if (models == null || models.isEmpty()) return Collections.emptyList();
        @Nullable Collection<M> resultModels;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModels = modelRepository.set(models);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultModels;
    }

    @Override
    public void clear() throws AbstractException {
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            modelRepository.clear();
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractException {
        @Nullable List<M> resultModels;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModels = modelRepository.findAll();
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultModels;
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) throws AbstractException {
        if (comparator == null) return findAll();
        @Nullable List<M> resultModels;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModels = modelRepository.findAll(comparator);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultModels;
    }

    @NotNull
    @Override
    public M findOneById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModel = modelRepository.findOneById(id);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public boolean existsById(@Nullable final String id) throws AbstractException {
        try {
            findOneById(id);
        } catch (EntityNotFoundException e) {
            return false;
        }
        return true;
    }

    @NotNull
    @Override
    public M removeOne(@Nullable final M model) throws AbstractException {
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModel = modelRepository.removeOne(model);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @NotNull
    @Override
    public M removeById(@Nullable final String id) throws AbstractException {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModel = modelRepository.removeById(id);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @Override
    public int getSize() throws AbstractException {
        @NotNull final Connection connection = getConnection();
        int result = 0;
        try {
            @NotNull final R modelRepository = getRepository(connection);
            result = modelRepository.getSize();
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return result;
    }

}
