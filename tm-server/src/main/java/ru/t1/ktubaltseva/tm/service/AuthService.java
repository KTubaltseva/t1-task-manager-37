package ru.t1.ktubaltseva.tm.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.service.IAuthService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.ISessionService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AccessDeniedException;
import ru.t1.ktubaltseva.tm.exception.auth.PasswordIncorrectException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.AbstractEntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.PasswordEmptyException;
import ru.t1.ktubaltseva.tm.model.Session;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.util.CryptUtil;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.util.Date;

public final class AuthService implements IAuthService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final ISessionService sessionService;

    public AuthService(
            @NotNull final IUserService userService,
            @NotNull final IPropertyService propertyService,
            @NotNull final ISessionService sessionService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
        this.sessionService = sessionService;
    }

    @NotNull
    @Override
    public String login(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException, JsonProcessingException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final User user = userService.findByLogin(login);
        if (user.isLocked()) throw new AccessDeniedException();
        @NotNull final String hash = HashUtil.salt(propertyService, password);
        if (!hash.equals(user.getPasswordHash())) throw new PasswordIncorrectException();
        return getToken(user);
    }

    @NotNull
    private Session createSession(
            @Nullable final User user
    ) throws AbstractEntityNotFoundException, SqlDataException {
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        @NotNull final Role role = user.getRole();
        @NotNull final Session session = new Session();
        session.setUserId(userId);
        session.setRole(role);
        return sessionService.add(session);
    }

    @NotNull
    private String getToken(
            @Nullable final User user
    ) throws AbstractException, JsonProcessingException {
        return getToken(createSession(user));
    }

    @NotNull
    private String getToken(
            @Nullable final Session session
    ) throws JsonProcessingException {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    @Override
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException, NoSuchAlgorithmException {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    @SneakyThrows
    public Session validateToken(
            @Nullable final String token
    ) {
        if (token == null) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Session session = objectMapper.readValue(json, Session.class);

        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getCreated();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        final long timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();

        if (!sessionService.existsById(session.getId())) throw new AccessDeniedException();
        return session;
    }

    @Override
    public void invalidate(
            @Nullable final Session session
    ) throws AbstractException {
        if (session == null) return;
        sessionService.removeById(session.getId());
    }

}
