package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IRepository;
import ru.t1.ktubaltseva.tm.comparator.CreatedComparator;
import ru.t1.ktubaltseva.tm.comparator.NameComparator;
import ru.t1.ktubaltseva.tm.comparator.StatusComparator;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    protected final Connection connection;

    public AbstractRepository(@NotNull final Connection connection) {
        this.connection = connection;
    }

    protected abstract String getTableName();

    @NotNull
    protected abstract M fetch(@NotNull final ResultSet row) throws SQLException, AbstractFieldException;

    @NotNull
    @Override
    public Collection<M> add(@NotNull final Collection<M> models) throws SQLException {
        List<M> result = new ArrayList<>();
        for (M model : models) {
            result.add(add(model));
        }
        return result;
    }

    @Override
    public void clear() throws SQLException, AbstractFieldException {
        @NotNull final Collection<M> models = findAll();
        for (M model : models) {
            removeOne(model);
        }
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) throws SQLException, AbstractFieldException {
        clear();
        return add(models);
    }

    @Override
    public boolean existsById(@NotNull final String id) throws SQLException, AbstractFieldException {
        return findOneById(id) != null;
    }

    @NotNull
    @Override
    public List<M> findAll() throws AbstractFieldException, SQLException {
        @NotNull final String tableName = getTableName();
        @NotNull final List<M> resultModels = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) resultModels.add(fetch(resultSet));
        }
        return resultModels;
    }

    @NotNull
    @Override
    public List<M> findAll(@NotNull final Comparator<M> comparator) throws AbstractFieldException, SQLException {
        @NotNull final String tableName = getTableName();
        @NotNull final String sortColumnName = getSortColumnName(comparator);
        @NotNull final List<M> resultModels = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s order by %s;", tableName, sortColumnName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) resultModels.add(fetch(resultSet));
        }
        return resultModels;
    }

    @NotNull
    protected String getSortColumnName(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == NameComparator.INSTANCE) return "name";
        if (comparator == StatusComparator.INSTANCE) return "status";
        return "created";
    }

    @Nullable
    @Override
    public M findOneById(@NotNull final String id) throws SQLException, AbstractFieldException {
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("select * from %s where id = ? limit 1;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Override
    public int getSize() throws SQLException {
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("select count(1) from %s;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return 0;
            return resultSet.getInt(1);
        }
    }

    @Override
    public void removeAll(@NotNull final List<M> models) throws SQLException, AbstractFieldException {
        for (M model : models) {
            removeOne(model);
        }
    }

    @Nullable
    @Override
    public M removeOne(@NotNull final M model) throws SQLException, AbstractFieldException {
        return removeById(model.getId());
    }

    @Nullable
    @Override
    public M removeById(@NotNull final String id) throws SQLException, AbstractFieldException {
        @Nullable final M resultModel = findOneById(id);
        if (resultModel == null) return null;
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("delete from %s where id = ?;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.executeUpdate();
        }
        return resultModel;
    }

}
