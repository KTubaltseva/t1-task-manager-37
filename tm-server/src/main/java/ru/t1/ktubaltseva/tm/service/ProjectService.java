package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IProjectService;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.field.DescriptionEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.StatusEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Project resultProject = findOneById(userId, id);
        resultProject.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository modelRepository = getRepository(connection);
            resultProject = modelRepository.update(resultProject);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultProject;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Project resultProject;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository modelRepository = getRepository(connection);
            resultProject = modelRepository.create(userId, name, description);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultProject;
    }

    @NotNull
    @Override
    public Project create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Project resultProject;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository modelRepository = getRepository(connection);
            resultProject = modelRepository.create(userId, name);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultProject;
    }

    @NotNull
    @Override
    public List<Project> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (sort == null) return findAll(userId);
        @Nullable List<Project> resultProjects;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository modelRepository = getRepository(connection);
            resultProjects = modelRepository.findAll(userId, (Comparator<Project>) sort.getComparator());
            connection.commit();
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultProjects;
    }

    @NotNull
    @Override
    public Project updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new ProjectIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Project resultProject = findOneById(userId, id);
        resultProject.setName(name);
        resultProject.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository modelRepository = getRepository(connection);
            resultProject = modelRepository.update(resultProject);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultProject;
    }

    @Override
    protected @NotNull IProjectRepository getRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

}
