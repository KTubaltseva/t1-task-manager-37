package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ISessionRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.EmailAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.LoginAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.EmailEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.LoginEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.PasswordEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.RoleEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.SessionRepository;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;
import ru.t1.ktubaltseva.tm.repository.UserRepository;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IPropertyService propertyService,
            @NotNull final IConnectionService connectionService) {
        super(connectionService);
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    protected IUserRepository getRepository(@NotNull final Connection connection) {
        return new UserRepository(connection);
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    @NotNull
    private ISessionRepository getSessionRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @Nullable User resultUser;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultUser = modelRepository.create(login, HashUtil.salt(propertyService, password));
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultUser;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (isEmailExists(login)) throw new EmailAlreadyExistsException(email);
        @Nullable User resultUser;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultUser = modelRepository.create(login, HashUtil.salt(propertyService, password), email);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultUser;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @Nullable User resultUser;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultUser = modelRepository.create(login, HashUtil.salt(propertyService, password), role);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultUser;
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultUser = modelRepository.findByLogin(login);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultUser;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultUser = modelRepository.findByEmail(email);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        if (resultUser == null) throw new UserNotFoundException();
        return resultUser;
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        Boolean result;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            result = modelRepository.isLoginExists(login);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return result;
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        Boolean result;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            result = modelRepository.isEmailExists(email);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return result;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(true);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultUser = modelRepository.update(resultUser);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultUser;
    }

    @NotNull
    @Override
    public User removeOne(@Nullable final User model) throws AbstractException {
        if (model == null) throw new UserNotFoundException();
        @NotNull final String userId = model.getId();
        @Nullable User resultUser;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            @NotNull final ISessionRepository sessionRepository = getSessionRepository(connection);
            @NotNull final List<Project> projects = projectRepository.findAll(userId);
            for (@NotNull final Project project : projects) {
                taskRepository.removeAllByProjectId(userId, project.getId());
            }
            projectRepository.clear(userId);
            sessionRepository.clear(userId);
            try {
                resultUser = super.removeOne(model);
            } catch (EntityNotFoundException e) {
                throw new UserNotFoundException();
            }
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultUser;
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultModel = findByLogin(login);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultModel = modelRepository.removeOne(resultModel);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) throws AbstractException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable User resultModel = findByEmail(email);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultModel = modelRepository.removeOne(resultModel);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable User resultUser = findOneById(id);
        resultUser.setPasswordHash(HashUtil.salt(propertyService, password));
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultUser = modelRepository.update(resultUser);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultUser;
    }

    @NotNull
    @Override
    public User unlockUserByLogin(@Nullable final String login) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable User resultUser = findByLogin(login);
        resultUser.setLocked(false);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultUser = modelRepository.update(resultUser);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultUser;
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) throws AbstractException {
        @Nullable User resultUser = findOneById(id);
        resultUser.setFirstName(firstName);
        resultUser.setMiddleName(middleName);
        resultUser.setLastName(lastName);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IUserRepository modelRepository = getRepository(connection);
            resultUser = modelRepository.update(resultUser);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultUser;
    }

}
