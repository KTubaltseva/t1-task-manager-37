package ru.t1.ktubaltseva.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IDatabaseProperty;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NotNull
    private final IDatabaseProperty databaseProperty;

    public ConnectionService(@NotNull final IDatabaseProperty databasePropertyService) {
        this.databaseProperty = databasePropertyService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Connection getConnection() {
        @NotNull final Connection connection = DriverManager.getConnection(
                databaseProperty.getDatabaseURL(),
                databaseProperty.getDatabaseUsername(),
                databaseProperty.getDatabasePassword()
        );
        connection.setAutoCommit(false);
        return connection;
    }

}
