package ru.t1.ktubaltseva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.model.Project;

import java.sql.SQLException;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws SQLException;

    @NotNull
    Project create(
            @NotNull String userId,
            @NotNull String name
    ) throws SQLException;

}
