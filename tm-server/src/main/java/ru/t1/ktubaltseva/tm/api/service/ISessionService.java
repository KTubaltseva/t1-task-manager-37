package ru.t1.ktubaltseva.tm.api.service;

import ru.t1.ktubaltseva.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {
}
