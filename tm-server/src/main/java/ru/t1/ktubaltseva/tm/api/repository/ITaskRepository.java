package ru.t1.ktubaltseva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.model.Task;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name,
            @NotNull String description
    ) throws SQLException;

    @NotNull
    Task create(
            @NotNull String userId,
            @NotNull String name
    ) throws SQLException;

    @NotNull
    List<Task> findAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    ) throws SQLException, AbstractFieldException;

    void removeAllByProjectId(
            @NotNull String userId,
            @NotNull String projectId
    ) throws SQLException, AbstractException;

}
