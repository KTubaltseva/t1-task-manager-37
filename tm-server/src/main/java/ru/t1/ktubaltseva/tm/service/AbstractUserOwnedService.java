package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IUserOwnedRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IUserOwnedService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public M add(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModel = modelRepository.add(userId, model);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultModel;
    }

    @Override
    public void clear(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            modelRepository.clear(userId);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @Nullable List<M> resultModels;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModels = modelRepository.findAll(userId);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultModels;
    }

    @NotNull
    @Override
    public List<M> findAll(
            @Nullable final String userId,
            @Nullable final Comparator<M> comparator
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (comparator == null) return findAll(userId);
        @Nullable List<M> resultModels;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModels = modelRepository.findAll(userId, comparator);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultModels;
    }

    @NotNull
    @Override
    public M findOneById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModel = modelRepository.findOneById(userId, id);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @NotNull
    @Override
    public M removeOne(
            @Nullable final String userId,
            @Nullable final M model
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (model == null) throw new EntityNotFoundException();
        @Nullable M resultModel;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModel = modelRepository.removeOne(userId, model);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

    @NotNull
    @Override
    public M removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable M resultModel;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final R modelRepository = getRepository(connection);
            resultModel = modelRepository.removeById(userId, id);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        if (resultModel == null) throw new EntityNotFoundException();
        return resultModel;
    }

}
