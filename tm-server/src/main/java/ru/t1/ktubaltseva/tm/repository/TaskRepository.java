package ru.t1.ktubaltseva.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.model.Task;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Getter
    private final String tableName = "tm_task";

    public TaskRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Task add(@NotNull final Task task) throws SQLException {
        @NotNull final String sql = String.format(
                "insert into %s (id, created, user_id, project_id, name, description, status) " +
                        "values (?, ?, ?, ?, ?, ?, ?)",
                tableName
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, task.getId());
            statement.setTimestamp(2, new Timestamp(task.getCreated().getTime()));
            statement.setString(3, task.getUserId());
            statement.setString(4, task.getProjectId());
            statement.setString(5, task.getName());
            statement.setString(6, task.getDescription());
            statement.setString(7, task.getStatus().toString());
            statement.executeUpdate();
        }
        return task;
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws SQLException {
        @NotNull final Task task = new Task(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name
    ) throws SQLException {
        @NotNull final Task task = new Task(name);
        return add(userId, task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException, AbstractFieldException {
        if (userId.isEmpty()) return Collections.emptyList();
        if (projectId.isEmpty()) return Collections.emptyList();
        @NotNull final String tableName = getTableName();
        @NotNull final List<Task> resultTasks = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s where user_id = ? and project_id = ?;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            statement.setString(2, projectId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) resultTasks.add(fetch(resultSet));
        }
        return resultTasks;
    }

    @Override
    public void removeAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) throws SQLException, AbstractFieldException {
        final List<Task> tasks = findAllByProjectId(userId, projectId);
        removeAll(tasks);
    }

    @NotNull
    @Override
    protected Task fetch(@NotNull final ResultSet row) throws SQLException, AbstractFieldException {
        @NotNull final Task task = new Task();
        task.setId(row.getString("id"));
        task.setCreated(row.getTimestamp("created"));
        task.setUserId(row.getString("user_id"));
        task.setProjectId(row.getString("project_id"));
        task.setName(row.getString("name"));
        task.setDescription(row.getString("description"));
        task.setStatus(Status.toStatus(row.getString("status")));
        return task;
    }

    @NotNull
    @Override
    public Task update(@NotNull final Task task) throws SQLException {
        @NotNull final String sql = String.format(
                "update %s " +
                        "set created = ?, " +
                        "user_id = ?, " +
                        "project_id = ?, " +
                        "name = ?, " +
                        "description = ?, " +
                        "status = ? " +
                        "where id = ?;",
                tableName
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(task.getCreated().getTime()));
            statement.setString(2, task.getUserId());
            statement.setString(3, task.getProjectId());
            statement.setString(4, task.getName());
            statement.setString(5, task.getDescription());
            statement.setString(6, task.getStatus().toString());
            statement.setString(7, task.getId());
            statement.executeUpdate();
        }
        return task;
    }

}
