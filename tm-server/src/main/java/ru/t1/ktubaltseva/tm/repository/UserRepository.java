package ru.t1.ktubaltseva.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.model.User;

import java.sql.*;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Getter
    private final String tableName = "tm_user";

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull String passwordHash) throws SQLException {
        @NotNull final User user = new User(login, passwordHash);
        return add(user);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String passwordHash, @NotNull final String email) throws SQLException {
        @NotNull final User user = new User(login, passwordHash, email);
        return add(user);
    }

    @NotNull
    @Override
    public User create(@NotNull final String login, @NotNull final String passwordHash, @NotNull final Role role) throws SQLException {
        @NotNull final User user = new User(login, passwordHash, role);
        return add(user);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) throws SQLException, AbstractFieldException {
        @NotNull final String sql = String.format("select * from %s where login = ? limit 1;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @Nullable
    @Override
    public User findByEmail(@NotNull final String email) throws SQLException, AbstractFieldException {
        @NotNull final String sql = String.format("select * from %s where email = ? limit 1;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@NotNull final String login) throws SQLException, AbstractFieldException {
        return findByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@NotNull final String email) throws SQLException, AbstractFieldException {
        return findByEmail(email) != null;
    }

    @Override
    protected @NotNull User fetch(@NotNull final ResultSet row) throws SQLException, AbstractFieldException {
        @NotNull final User user = new User();
        user.setId(row.getString("id"));
        user.setCreated(row.getTimestamp("created"));
        user.setLogin(row.getString("login"));
        user.setPasswordHash(row.getString("passwordHash"));
        user.setEmail(row.getString("email"));
        user.setFirstName(row.getString("firstName"));
        user.setMiddleName(row.getString("middleName"));
        user.setLastName(row.getString("lastName"));
        user.setRole(Role.toRole(row.getString("role")));
        user.setLocked(row.getBoolean("locked"));
        return user;
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) throws SQLException {
        @NotNull final String sql = String.format(
                "insert into %s (id, created, login, passwordHash, email, firstName, middleName, lastName, role, locked) " +
                        "values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                tableName
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setTimestamp(2, new Timestamp(user.getCreated().getTime()));
            statement.setString(3, user.getLogin());
            statement.setString(4, user.getPasswordHash());
            statement.setString(5, user.getEmail());
            statement.setString(6, user.getFirstName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getLastName());
            statement.setString(9, user.getRole().toString());
            statement.setBoolean(10, user.isLocked());
            statement.executeUpdate();
        }
        return user;
    }

    @NotNull
    @Override
    public User update(@NotNull final User user) throws SQLException {
        @NotNull final String sql = String.format(
                "update %s " +
                        "set created = ?, " +
                        "login = ?, " +
                        "passwordHash = ?, " +
                        "email = ?, " +
                        "firstName = ?, " +
                        "middleName = ?, " +
                        "lastName = ?, " +
                        "role = ?, " +
                        "locked = ? " +
                        "where id = ?;",
                tableName
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(user.getCreated().getTime()));
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getMiddleName());
            statement.setString(7, user.getLastName());
            statement.setString(8, user.getRole().toString());
            statement.setBoolean(9, user.isLocked());
            statement.setString(10, user.getId());
            statement.executeUpdate();
        }
        return user;
    }

}
