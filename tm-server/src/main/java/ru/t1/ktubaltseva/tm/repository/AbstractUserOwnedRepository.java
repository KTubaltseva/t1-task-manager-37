package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IUserOwnedRepository;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    public AbstractUserOwnedRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    public M add(
            @NotNull final String userId,
            @NotNull final M model
    ) throws SQLException {
        model.setUserId(userId);
        return add(model);
    }

    public void clear(@NotNull final String userId) throws SQLException, AbstractFieldException {
        @NotNull final List<M> userModelsRemoved = findAll(userId);
        removeAll(userModelsRemoved);
    }

    public boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException, AbstractFieldException {
        return findOneById(userId, id) != null;
    }

    @NotNull
    public List<M> findAll(@NotNull final String userId) throws SQLException, AbstractFieldException {
        @NotNull final String tableName = getTableName();
        @NotNull final List<M> resultModels = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s where user_id = ?;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) resultModels.add(fetch(resultSet));
        }
        return resultModels;
    }

    @NotNull
    public List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<M> comparator
    ) throws SQLException, AbstractFieldException {
        @NotNull final String tableName = getTableName();
        @NotNull final String sortColumnName = getSortColumnName(comparator);
        @NotNull final List<M> resultModels = new ArrayList<>();
        @NotNull final String sql = String.format("select * from %s where user_id = ? order by %s;", tableName, sortColumnName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) resultModels.add(fetch(resultSet));
        }
        return resultModels;
    }

    @Nullable
    public M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException, AbstractFieldException {
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("select * from %s where id = ? and user_id = ? limit 1;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }

    public int getSize(@NotNull final String userId) throws SQLException {
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("select count(1) where user_id = ? from %s;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, userId);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return 0;
            return resultSet.getInt(1);
        }
    }

    public void removeAll(@NotNull final String userId) throws SQLException, AbstractFieldException {
        final List<M> models = findAll(userId);
        removeAll(models);
    }

    @Nullable
    public M removeOne(
            @NotNull final String userId,
            @NotNull final M model
    ) throws SQLException, AbstractFieldException {
        return removeById(userId, model.getId());
    }

    @Nullable
    public M removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws SQLException, AbstractFieldException {
        @Nullable final M resultModel = findOneById(userId, id);
        if (resultModel == null) return null;
        @NotNull final String tableName = getTableName();
        @NotNull final String sql = String.format("delete from %s where id = ? and user_id = ? ;", tableName);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, id);
            statement.setString(2, userId);
            statement.executeUpdate();
        }
        return resultModel;
    }

}
