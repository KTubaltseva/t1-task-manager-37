package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ITaskService;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Task resultTask;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository modelRepository = getRepository(connection);
            resultTask = modelRepository.create(userId, name, description);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultTask;
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable Task resultTask;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository modelRepository = getRepository(connection);
            resultTask = modelRepository.create(userId, name);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultTask;
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable Task resultTask = findOneById(userId, id);
        resultTask.setStatus(status);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository modelRepository = getRepository(connection);
            resultTask = modelRepository.update(resultTask);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultTask;
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (sort == null) return findAll(userId);
        @Nullable List<Task> resultTasks;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository modelRepository = getRepository(connection);
            resultTasks = modelRepository.findAll(userId, (Comparator<Task>) sort.getComparator());
            connection.commit();
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultTasks;
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        @Nullable List<Task> resultModels;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository modelRepository = getRepository(connection);
            resultModels = modelRepository.findAllByProjectId(userId, projectId);
        } catch (@NotNull final SQLException e) {
            throw new SqlDataException(e);
        } finally {
            connectionClose(connection);
        }
        return resultModels;
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (id == null || id.isEmpty()) throw new TaskIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable Task resultTask = findOneById(userId, id);
        resultTask.setName(name);
        resultTask.setDescription(description);
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final ITaskRepository modelRepository = getRepository(connection);
            resultTask = modelRepository.update(resultTask);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultTask;
    }

    @NotNull
    @Override
    protected ITaskRepository getRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

}
