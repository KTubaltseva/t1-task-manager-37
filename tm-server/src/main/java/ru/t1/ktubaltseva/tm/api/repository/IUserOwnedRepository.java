package ru.t1.ktubaltseva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    @NotNull
    M add(
            @NotNull String userId,
            @NotNull M model
    ) throws SQLException;

    void clear(@NotNull String userId) throws SQLException, AbstractFieldException;

    boolean existsById(
            @NotNull String userId,
            @NotNull String id
    ) throws SQLException, AbstractFieldException;

    @NotNull
    List<M> findAll(@NotNull String userId) throws SQLException, AbstractFieldException;

    @NotNull
    List<M> findAll(
            @NotNull String userId,
            @NotNull Comparator<M> comparator
    ) throws SQLException, AbstractFieldException;

    @Nullable
    M findOneById(
            @NotNull String userId,
            @NotNull String id
    ) throws SQLException, AbstractFieldException;

    int getSize(@NotNull String userId) throws SQLException;

    @Nullable
    M removeOne(
            @NotNull String userId,
            @NotNull M model
    ) throws SQLException, AbstractFieldException;

    @Nullable
    M removeById(
            @NotNull String userId,
            @NotNull String id
    ) throws SQLException, AbstractFieldException;

}
