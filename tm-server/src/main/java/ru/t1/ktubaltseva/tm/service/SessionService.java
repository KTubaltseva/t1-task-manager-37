package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.ISessionRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ISessionService;
import ru.t1.ktubaltseva.tm.model.Session;
import ru.t1.ktubaltseva.tm.repository.SessionRepository;

import java.sql.Connection;

public class SessionService extends AbstractUserOwnedService<Session, ISessionRepository> implements ISessionService {

    public SessionService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    protected @NotNull ISessionRepository getRepository(@NotNull final Connection connection) {
        return new SessionRepository(connection);
    }

}
