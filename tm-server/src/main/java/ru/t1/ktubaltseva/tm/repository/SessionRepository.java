package ru.t1.ktubaltseva.tm.repository;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.ISessionRepository;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.model.Session;

import java.sql.*;

public class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {

    @Getter
    private final String tableName = "tm_session";

    public SessionRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public Session add(@NotNull final Session session) throws SQLException {
        @NotNull final String sql = String.format(
                "insert into %s (id, created, user_id, role) " +
                        "values (?, ?, ?, ?)",
                tableName
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, session.getId());
            statement.setTimestamp(2, new Timestamp(session.getCreated().getTime()));
            statement.setString(3, session.getUserId());
            statement.setString(4, session.getRole().toString());
            statement.executeUpdate();
        }
        return session;
    }

    @NotNull
    @Override
    protected Session fetch(@NotNull final ResultSet row) throws SQLException, AbstractFieldException {
        @NotNull final Session session = new Session();
        session.setId(row.getString("id"));
        session.setCreated(row.getTimestamp("created"));
        session.setUserId(row.getString("user_id"));
        session.setRole(Role.toRole(row.getString("role")));
        return session;
    }

    @NotNull
    @Override
    public Session update(@NotNull final Session session) throws SQLException, AbstractFieldException {
        @NotNull final String sql = String.format(
                "update %s " +
                        "set created = ?, " +
                        "user_id = ?, " +
                        "role = ? " +
                        "where id = ?;",
                tableName
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setTimestamp(1, new Timestamp(session.getCreated().getTime()));
            statement.setString(2, session.getUserId());
            statement.setString(3, session.getRole().toString());
            statement.setString(4, session.getId());
            statement.executeUpdate();
        }
        return session;
    }

}
