package ru.t1.ktubaltseva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.model.User;

import java.sql.SQLException;

public interface IUserRepository extends IRepository<User> {

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String passwordHash
    ) throws SQLException;

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String passwordHash,
            @NotNull String email
    ) throws SQLException;

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String passwordHash,
            @NotNull Role role
    ) throws SQLException;

    @Nullable
    User findByLogin(@NotNull String login) throws SQLException, AbstractFieldException;

    @Nullable
    User findByEmail(@NotNull String email) throws SQLException, AbstractFieldException;

    @NotNull
    Boolean isLoginExists(@NotNull String login) throws SQLException, AbstractFieldException;

    @NotNull
    Boolean isEmailExists(@NotNull String email) throws SQLException, AbstractFieldException;

}
