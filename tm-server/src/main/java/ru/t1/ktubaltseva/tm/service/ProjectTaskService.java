package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.IProjectTaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.TaskIdEmptyException;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    protected final IConnectionService connectionService;

    public ProjectTaskService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    private IProjectRepository getProjectRepository(@NotNull final Connection connection) {
        return new ProjectRepository(connection);
    }

    @NotNull
    private ITaskRepository getTaskRepository(@NotNull final Connection connection) {
        return new TaskRepository(connection);
    }

    protected void connectionRollback(@NotNull final Connection connection) throws SqlDataException {
        try {
            connection.rollback();
        } catch (SQLException e) {
            throw new SqlDataException(e);
        }
    }

    protected void connectionClose(@NotNull final Connection connection) throws SqlDataException {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new SqlDataException(e);
        }
    }

    @NotNull
    protected Connection getConnection() {
        return connectionService.getConnection();
    }

    @NotNull
    @Override
    public Task bindTaskToProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable Task resultTask;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            resultTask = taskRepository.findOneById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProjectId(projectId);
            resultTask = taskRepository.update(resultTask);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultTask;
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(
            @Nullable final String userId,
            @Nullable final String projectId,
            @Nullable final String taskId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        @Nullable Task resultTask;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            if (!projectRepository.existsById(userId, projectId)) throw new ProjectNotFoundException();
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            resultTask = taskRepository.findOneById(userId, taskId);
            if (resultTask == null) throw new TaskNotFoundException();
            resultTask.setProjectId(null);
            resultTask = taskRepository.update(resultTask);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        return resultTask;
    }

    @Override
    public @NotNull Project removeProjectById(
            @Nullable final String userId,
            @Nullable final String projectId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new IdEmptyException();
        @Nullable Project resultProject;
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            resultProject = projectRepository.findOneById(userId, projectId);
            if (resultProject == null) throw new ProjectNotFoundException();
            taskRepository.removeAllByProjectId(userId, projectId);
            resultProject = projectRepository.removeById(userId, projectId);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
        if (resultProject == null) throw new ProjectNotFoundException();
        return resultProject;
    }

    @Override
    public void clearProjects(@Nullable final String userId) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        @NotNull final Connection connection = getConnection();
        try {
            @NotNull final IProjectRepository projectRepository = getProjectRepository(connection);
            @NotNull final ITaskRepository taskRepository = getTaskRepository(connection);
            @NotNull final List<Project> projects = projectRepository.findAll(userId);
            for (@NotNull final Project project : projects) {
                taskRepository.removeAllByProjectId(userId, project.getId());
            }
            projectRepository.clear(userId);
            connection.commit();
        } catch (@NotNull final SQLException e) {
            connectionRollback(connection);
            throw new SqlDataException(e);
        } catch (@NotNull final AbstractException e) {
            connectionRollback(connection);
            throw e;
        } finally {
            connectionClose(connection);
        }
    }

}
