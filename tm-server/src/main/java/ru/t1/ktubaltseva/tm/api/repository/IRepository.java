package ru.t1.ktubaltseva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.data.SqlDataException;
import ru.t1.ktubaltseva.tm.exception.field.AbstractFieldException;
import ru.t1.ktubaltseva.tm.model.AbstractModel;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M update(@NotNull M model) throws SQLException, AbstractFieldException;

    @NotNull
    M add(@NotNull M model) throws SQLException;

    @NotNull
    Collection<M> add(@NotNull Collection<M> models) throws SQLException;

    @NotNull
    Collection<M> set(@NotNull Collection<M> models) throws SQLException, AbstractFieldException;

    void clear() throws SqlDataException, SQLException, AbstractFieldException;

    boolean existsById(@NotNull String id) throws SQLException, AbstractFieldException;

    @NotNull
    List<M> findAll() throws AbstractException, SQLException;

    @NotNull
    List<M> findAll(@NotNull Comparator<M> comparator) throws AbstractException, SQLException;

    @Nullable
    M findOneById(@NotNull String id) throws SQLException, AbstractFieldException;

    int getSize() throws SQLException;

    void removeAll(@NotNull List<M> models) throws SQLException, AbstractFieldException;

    @Nullable
    M removeOne(@NotNull M model) throws SQLException, AbstractFieldException;

    @Nullable
    M removeById(@NotNull String id) throws SQLException, AbstractFieldException;

}
