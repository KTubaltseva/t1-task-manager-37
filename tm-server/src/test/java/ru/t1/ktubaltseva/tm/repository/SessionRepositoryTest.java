package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.ISessionRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Session;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.service.ConnectionService;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;
import ru.t1.ktubaltseva.tm.service.UserService;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.SessionTestData.*;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final ISessionRepository repository = new SessionRepository(connection);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        connection.setAutoCommit(true);
        userService.add(USER_1);
        userService.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
        connection.close();
    }

    @After
    @SneakyThrows
    public void after() {
        repository.removeAll(SESSION_LIST);
        repository.clear(USER_1.getId());
        repository.clear(USER_2.getId());
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final Session sessionToAdd = USER_1_SESSION_1;
        @Nullable final String sessionToAddId = sessionToAdd.getId();

        @Nullable final Session sessionAdded = repository.add((sessionToAdd));
        Assert.assertNotNull(sessionAdded);
        Assert.assertEquals(sessionToAdd.getId(), sessionAdded.getId());

        @Nullable final Session sessionFindOneById = repository.findOneById(sessionToAddId);
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionToAdd.getId(), sessionFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final Session sessionToAddByUser = USER_1_SESSION_1;
        @Nullable final String sessionToAddByUserId = sessionToAddByUser.getId();

        @Nullable final Session sessionAddedByUser = repository.add(userToAddId, sessionToAddByUser);
        Assert.assertNotNull(sessionAddedByUser);
        Assert.assertTrue(repository.existsById(sessionToAddByUserId));

        @Nullable final Session sessionFindOneById = repository.findOneById(sessionToAddByUserId);
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionAddedByUser.getId(), sessionFindOneById.getId());

        @Nullable final Session sessionFindOneByIdByUserIdToAdd = repository.findOneById(userToAddId, sessionToAddByUserId);
        Assert.assertNotNull(sessionFindOneByIdByUserIdToAdd);
        Assert.assertEquals(sessionAddedByUser.getId(), sessionFindOneByIdByUserIdToAdd.getId());

        @Nullable final Session sessionFindOneByIdByUserIdNoAdd = repository.findOneById(userNoAddId, sessionToAddByUserId);
        Assert.assertNull(sessionFindOneByIdByUserIdNoAdd);
    }

    @Test
    @SneakyThrows
    public void addMany() {
        @Nullable final Collection<Session> sessionList = repository.add(SESSION_LIST);
        Assert.assertNotNull(sessionList);
        for (@NotNull final Session session : SESSION_LIST) {
            @Nullable final Session sessionFindOneById = repository.findOneById(session.getId());
            Assert.assertNotNull(sessionFindOneById);
            Assert.assertEquals(session.getId(), sessionFindOneById.getId());
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final Session sessionExists = USER_1_SESSION_1;
        repository.add(sessionExists);

        @Nullable final Session sessionFindOneById = repository.findOneById(sessionExists.getId());
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionExists.getId(), sessionFindOneById.getId());

        @Nullable final Session sessionFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_SESSION_ID);
        Assert.assertNull(sessionFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final Session sessionExists = USER_1_SESSION_1;
        @NotNull final User userExists = USER_1;
        repository.add(userExists.getId(), sessionExists);

        Assert.assertNull(repository.findOneById(userExists.getId(), NON_EXISTENT_SESSION_ID));
        Assert.assertNull(repository.findOneById(NON_EXISTENT_USER_ID, sessionExists.getId()));

        @Nullable final Session sessionFindOneById = repository.findOneById(userExists.getId(), sessionExists.getId());
        Assert.assertNotNull(sessionFindOneById);
        Assert.assertEquals(sessionExists.getId(), sessionFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final Session sessionExists = USER_1_SESSION_1;

        repository.clear();
        @NotNull final Collection<Session> sessionsFindAllEmpty = repository.findAll();
        Assert.assertNotNull(sessionsFindAllEmpty);
        Assert.assertEquals(Collections.emptyList(), sessionsFindAllEmpty);

        repository.add(sessionExists);
        @NotNull final Collection<Session> sessionsFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(sessionsFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final Session sessionExists = USER_1_SESSION_1;
        @NotNull final User userExists = USER_1;

        repository.add(userExists.getId(), sessionExists);
        @NotNull final Collection<Session> sessionsFindAllByUserRepNoEmpty = repository.findAll(userExists.getId());
        Assert.assertNotNull(sessionsFindAllByUserRepNoEmpty);

        @NotNull final Collection<Session> sessionsFindAllByNonExistentUser = repository.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(sessionsFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), sessionsFindAllByNonExistentUser);
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<Session> sessionByUserToClearList = repository.add(USER_1_SESSION_LIST);
        @NotNull final Collection<Session> sessionByUserNoClearList = repository.add(USER_2_SESSION_LIST);
        repository.clear(userToClearId);

        for (@NotNull final Session sessionByUserToClear : sessionByUserToClearList) {
            @Nullable final Session sessionFindOneById = repository.findOneById(sessionByUserToClear.getId());
            Assert.assertNull(sessionFindOneById);
        }
        Assert.assertEquals(0, repository.findAll(userToClearId).size());

        for (@NotNull final Session sessionByUserNoClear : sessionByUserNoClearList) {
            @Nullable final Session sessionFindOneById = repository.findOneById(sessionByUserNoClear.getId());
            Assert.assertEquals(sessionByUserNoClear.getId(), sessionFindOneById.getId());
        }
        Assert.assertNotEquals(0, repository.findAll(userNoClearId).size());
    }

    @Test
    @SneakyThrows
    public void removeOne() {
        @Nullable final Session sessionToRemove = USER_1_SESSION_1;
        repository.add((sessionToRemove));

        @Nullable final Session sessionRemoved = repository.removeOne(sessionToRemove);
        Assert.assertNotNull(sessionRemoved);
        Assert.assertEquals(sessionToRemove.getId(), sessionRemoved.getId());

        @Nullable final Session sessionFindOneById = repository.findOneById(sessionRemoved.getId());
        Assert.assertNull(sessionFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeOneByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Session sessionByUserToRemove = repository.add((USER_1_SESSION_1));
        @Nullable final Session sessionByUserNoRemove = repository.add((USER_2_SESSION_1));

        Assert.assertNull(repository.removeOne(NON_EXISTENT_SESSION_ID, sessionByUserToRemove));
        Assert.assertNull(repository.removeOne(userToRemoveId, NON_EXISTENT_SESSION));

        @Nullable final Session sessionRemoved = repository.removeOne(userToRemoveId, sessionByUserToRemove);
        Assert.assertNotNull(sessionRemoved);
        Assert.assertEquals(sessionRemoved.getId(), sessionByUserToRemove.getId());
        @Nullable final Session sessionRemovedFindOneById = repository.findOneById(sessionByUserToRemove.getId());
        Assert.assertNull(sessionRemovedFindOneById);

        @Nullable final Session sessionNoRemoved = repository.removeOne(userToRemoveId, sessionByUserNoRemove);
        Assert.assertNull(sessionNoRemoved);
        @Nullable final Session sessionNoRemovedFindOneById = repository.findOneById(sessionByUserNoRemove.getId());
        Assert.assertNotNull(sessionNoRemovedFindOneById);
        Assert.assertEquals(sessionNoRemovedFindOneById.getId(), sessionByUserNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final Session sessionToRemove = USER_1_SESSION_1;
        repository.add((sessionToRemove));

        Assert.assertNull(repository.removeById(NON_EXISTENT_SESSION_ID));

        @Nullable final Session sessionRemoved = repository.removeById(sessionToRemove.getId());
        Assert.assertNotNull(sessionRemoved);
        Assert.assertEquals(sessionToRemove.getId(), sessionRemoved.getId());

        @Nullable final Session sessionFindOneById = repository.findOneById(sessionRemoved.getId());
        Assert.assertNull(sessionFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Session sessionByUserToRemove = repository.add((USER_1_SESSION_1));
        @Nullable final Session sessionByUserNoRemove = repository.add((USER_2_SESSION_1));

        Assert.assertNull(repository.removeById(NON_EXISTENT_SESSION_ID, sessionByUserToRemove.getId()));
        Assert.assertNull(repository.removeById(userToRemoveId, NON_EXISTENT_SESSION_ID));

        @Nullable final Session sessionRemoved = repository.removeById(userToRemoveId, sessionByUserToRemove.getId());
        Assert.assertNotNull(sessionRemoved);
        Assert.assertEquals(sessionRemoved.getId(), sessionByUserToRemove.getId());
        @Nullable final Session sessionRemovedFindOneById = repository.findOneById(sessionByUserToRemove.getId());
        Assert.assertNull(sessionRemovedFindOneById);

        @Nullable final Session sessionNoRemoved = repository.removeById(userToRemoveId, sessionByUserNoRemove.getId());
        Assert.assertNull(sessionNoRemoved);
        @Nullable final Session sessionNoRemovedFindOneById = repository.findOneById(sessionByUserNoRemove.getId());
        Assert.assertNotNull(sessionNoRemovedFindOneById);
        Assert.assertEquals(sessionNoRemovedFindOneById.getId(), sessionByUserNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void isExists() {
        @NotNull final Session sessionExists = USER_1_SESSION_1;
        repository.add(sessionExists);

        Assert.assertFalse(repository.existsById(NON_EXISTENT_SESSION_ID));
        Assert.assertTrue(repository.existsById(sessionExists.getId()));
    }

}
