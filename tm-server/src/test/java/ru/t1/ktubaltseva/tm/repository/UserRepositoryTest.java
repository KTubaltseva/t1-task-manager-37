package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.service.ConnectionService;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;

import java.sql.Connection;
import java.util.Collection;

import static ru.t1.ktubaltseva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final IUserRepository repository = new UserRepository(connection);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        connection.setAutoCommit(true);
    }

    @After
    @SneakyThrows
    public void after() {
        repository.removeAll(USER_LIST);
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final User userToAdd = USER_1;
        @Nullable final String userToAddId = userToAdd.getId();

        @Nullable final User userAdded = repository.add((userToAdd));
        Assert.assertNotNull(userAdded);
        Assert.assertEquals(userToAdd, userAdded);

        @Nullable final User userFindOneById = repository.findOneById(userToAddId);
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userToAdd.getId(), userFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void addMany() {
        @Nullable final Collection<User> userList = repository.add(USER_LIST);
        Assert.assertNotNull(userList);
        for (@NotNull final User user : USER_LIST) {
            @Nullable final User userFindOneById = repository.findOneById(user.getId());
            Assert.assertNotNull(userFindOneById);
            Assert.assertEquals(user.getId(), userFindOneById.getId());
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final User userExists = USER_1;
        repository.add(userExists);

        @Nullable final User userFindOneById = repository.findOneById(userExists.getId());
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userExists.getId(), userFindOneById.getId());

        @Nullable final User userFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_USER_ID);
        Assert.assertNull(userFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findByEmail() {
        @NotNull final User userWithEmail = USER_WITH_LOGIN_EMAIL;
        repository.add(userWithEmail);

        @Nullable final User userFindByNotExistentEmail = repository.findByEmail(NON_EXISTENT_USER_EMAIL);
        Assert.assertNull(userFindByNotExistentEmail);

        @Nullable final User userFindByEmail = repository.findByEmail(USER_EMAIL);
        Assert.assertNotNull(userFindByEmail);
        Assert.assertEquals(userWithEmail.getId(), userFindByEmail.getId());
    }

    @Test
    @SneakyThrows
    public void findByLogin() {
        @NotNull final User userWithLogin = USER_WITH_LOGIN_EMAIL;
        repository.add(userWithLogin);

        @Nullable final User userFindByNotExistentLogin = repository.findByLogin(NON_EXISTENT_USER_LOGIN);
        Assert.assertNull(userFindByNotExistentLogin);

        @Nullable final User userFindByLogin = repository.findByLogin(USER_LOGIN);
        Assert.assertNotNull(userFindByLogin);
        Assert.assertEquals(userWithLogin.getId(), userFindByLogin.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final User userExists = USER_1;

        repository.add(userExists);
        @NotNull final Collection<User> usersFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(usersFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void removeOne() {
        @Nullable final User userToRemove = USER_1;
        repository.add((userToRemove));

        @Nullable final User userRemoved = repository.removeOne(userToRemove);
        Assert.assertNotNull(userRemoved);
        Assert.assertEquals(userToRemove.getId(), userRemoved.getId());

        @Nullable final User userFindOneById = repository.findOneById(userRemoved.getId());
        Assert.assertNull(userFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final User userToRemove = USER_1;
        repository.add((userToRemove));

        Assert.assertNull(repository.removeById(NON_EXISTENT_USER_ID));

        @Nullable final User userRemoved = repository.removeById(userToRemove.getId());
        Assert.assertNotNull(userRemoved);
        Assert.assertEquals(userToRemove.getId(), userRemoved.getId());

        @Nullable final User userFindOneById = repository.findOneById(userRemoved.getId());
        Assert.assertNull(userFindOneById);
    }

    @Test
    @SneakyThrows
    public void isEmailExists() {
        @NotNull final User userWithEmail = USER_WITH_LOGIN_EMAIL;
        repository.add(userWithEmail);

        Assert.assertFalse(repository.isEmailExists(NON_EXISTENT_USER_EMAIL));
        Assert.assertTrue(repository.isEmailExists(USER_EMAIL));
    }

    @Test
    @SneakyThrows
    public void isLoginExists() {
        @NotNull final User userWithLogin = USER_WITH_LOGIN_EMAIL;
        repository.add(userWithLogin);

        Assert.assertFalse(repository.isLoginExists(NON_EXISTENT_USER_LOGIN));
        Assert.assertTrue(repository.isLoginExists(USER_LOGIN));
    }

    @Test
    @SneakyThrows
    public void isExists() {
        @NotNull final User userExists = USER_1;
        repository.add(userExists);

        Assert.assertFalse(repository.existsById(NON_EXISTENT_USER_ID));
        Assert.assertTrue(repository.existsById(userExists.getId()));
    }

}
