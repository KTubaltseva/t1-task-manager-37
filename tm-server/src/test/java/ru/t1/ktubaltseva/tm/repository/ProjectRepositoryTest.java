package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.service.IConnectionService;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.service.ConnectionService;
import ru.t1.ktubaltseva.tm.service.LoggerService;
import ru.t1.ktubaltseva.tm.service.PropertyService;
import ru.t1.ktubaltseva.tm.service.UserService;

import java.sql.Connection;
import java.util.Collection;

import static ru.t1.ktubaltseva.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final IProjectRepository repository = new ProjectRepository(connection);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        connection.setAutoCommit(true);
        userService.add(USER_1);
        userService.add(USER_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
        connection.close();
    }

    @After
    @SneakyThrows
    public void after() {
        repository.removeAll(PROJECT_LIST);
        repository.clear(USER_1.getId());
        repository.clear(USER_2.getId());
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final Project projectToAdd = USER_1_PROJECT_1;
        @Nullable final String projectToAddId = projectToAdd.getId();

        @Nullable final Project projectAdded = repository.add((projectToAdd));

        Assert.assertNotNull(projectAdded);
        Assert.assertEquals(projectToAdd.getId(), projectAdded.getId());

        @Nullable final Project projectFindOneById = repository.findOneById(projectToAddId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectToAdd.getId(), projectFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final Project projectToAddByUser = USER_1_PROJECT_1;
        @Nullable final String projectToAddByUserId = projectToAddByUser.getId();

        @Nullable final Project projectAddedByUser = repository.add(userToAddId, projectToAddByUser);
        Assert.assertNotNull(projectAddedByUser);
        Assert.assertTrue(repository.existsById(projectToAddByUserId));

        @Nullable final Project projectFindOneById = repository.findOneById(projectToAddByUserId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectAddedByUser.getId(), projectFindOneById.getId());

        @Nullable final Project projectFindOneByIdByUserIdToAdd = repository.findOneById(userToAddId, projectToAddByUserId);
        Assert.assertNotNull(projectFindOneByIdByUserIdToAdd);
        Assert.assertEquals(projectAddedByUser.getId(), projectFindOneByIdByUserIdToAdd.getId());

        @Nullable final Project projectFindOneByIdByUserIdNoAdd = repository.findOneById(userNoAddId, projectToAddByUserId);
        Assert.assertNull(projectFindOneByIdByUserIdNoAdd);
    }

    @Test
    @SneakyThrows
    public void addMany() {
        @Nullable final Collection<Project> projectList = repository.add(PROJECT_LIST);
        Assert.assertNotNull(projectList);
        for (@NotNull final Project project : PROJECT_LIST) {
            @Nullable final Project projectFindOneById = repository.findOneById(project.getId());
            Assert.assertEquals(project.getId(), projectFindOneById.getId());
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        repository.add(projectExists);

        @Nullable final Project projectFindOneById = repository.findOneById(projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());

        @Nullable final Project projectFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_PROJECT_ID);
        Assert.assertNull(projectFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        @NotNull final User userExists = USER_1;
        repository.add(projectExists);

        Assert.assertNull(repository.findOneById(userExists.getId(), NON_EXISTENT_PROJECT_ID));
        Assert.assertNull(repository.findOneById(NON_EXISTENT_USER_ID, projectExists.getId()));

        @Nullable final Project projectFindOneById = repository.findOneById(userExists.getId(), projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists.getId(), projectFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        repository.add(projectExists);

        @NotNull final Collection<Project> projectsFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        @NotNull final User userExists = USER_1;
        repository.add(projectExists);

        @NotNull final Collection<Project> projectsFindAllByUserRepNoEmpty = repository.findAll(userExists.getId());
        Assert.assertNotNull(projectsFindAllByUserRepNoEmpty);
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<Project> projectByUserToClearList = repository.add(USER_1_PROJECT_LIST);
        @NotNull final Collection<Project> projectByUserNoClearList = repository.add(USER_2_PROJECT_LIST);
        repository.clear(userToClearId);

        for (@NotNull final Project projectByUserToClear : projectByUserToClearList) {
            @Nullable final Project projectFindOneById = repository.findOneById(projectByUserToClear.getId());
            Assert.assertNull(projectFindOneById);
        }
        Assert.assertEquals(0, repository.findAll(userToClearId).size());

        for (@NotNull final Project projectByUserNoClear : projectByUserNoClearList) {
            @Nullable final Project projectFindOneById = repository.findOneById(projectByUserNoClear.getId());
            Assert.assertEquals(projectByUserNoClear.getId(), projectFindOneById.getId());
        }
        Assert.assertNotEquals(0, repository.findAll(userNoClearId).size());
    }

    @Test
    @SneakyThrows
    public void removeOne() {
        @Nullable final Project projectToRemove = USER_1_PROJECT_1;
        repository.add((projectToRemove));

        @Nullable final Project projectRemoved = repository.removeOne(projectToRemove);
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectToRemove.getId(), projectRemoved.getId());

        @Nullable final Project projectFindOneById = repository.findOneById(projectRemoved.getId());
        Assert.assertNull(projectFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeOneByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Project projectByUserToRemove = repository.add((USER_1_PROJECT_1));
        @Nullable final Project projectByUserNoRemove = repository.add((USER_2_PROJECT_1));

        Assert.assertNull(repository.removeOne(NON_EXISTENT_USER_ID, projectByUserToRemove));
        Assert.assertNull(repository.removeOne(userToRemoveId, NON_EXISTENT_PROJECT));

        @Nullable final Project projectRemoved = repository.removeOne(userToRemoveId, projectByUserToRemove);
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectRemoved.getId(), projectByUserToRemove.getId());
        @Nullable final Project projectRemovedFindOneById = repository.findOneById(projectByUserToRemove.getId());
        Assert.assertNull(projectRemovedFindOneById);

        @Nullable final Project projectNoRemoved = repository.removeOne(userToRemoveId, projectByUserNoRemove);
        Assert.assertNull(projectNoRemoved);
        @Nullable final Project projectNoRemovedFindOneById = repository.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById.getId(), projectByUserNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final Project projectToRemove = USER_1_PROJECT_1;
        repository.add((projectToRemove));

        Assert.assertNull(repository.removeById(NON_EXISTENT_PROJECT_ID));

        @Nullable final Project projectRemoved = repository.removeById(projectToRemove.getId());
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectToRemove.getId(), projectRemoved.getId());

        @Nullable final Project projectFindOneById = repository.findOneById(projectRemoved.getId());
        Assert.assertNull(projectFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Project projectByUserToRemove = repository.add((USER_1_PROJECT_1));
        @Nullable final Project projectByUserNoRemove = repository.add((USER_2_PROJECT_1));

        Assert.assertNull(repository.removeById(NON_EXISTENT_USER_ID, projectByUserToRemove.getId()));
        Assert.assertNull(repository.removeById(userToRemoveId, NON_EXISTENT_PROJECT_ID));

        @Nullable final Project projectRemoved = repository.removeById(userToRemoveId, projectByUserToRemove.getId());
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectRemoved.getId(), projectByUserToRemove.getId());
        @Nullable final Project projectRemovedFindOneById = repository.findOneById(projectByUserToRemove.getId());
        Assert.assertNull(projectRemovedFindOneById);

        @Nullable final Project projectNoRemoved = repository.removeById(userToRemoveId, projectByUserNoRemove.getId());
        Assert.assertNull(projectNoRemoved);
        @Nullable final Project projectNoRemovedFindOneById = repository.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById.getId(), projectByUserNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void isExists() {
        @NotNull final Project projectExists = USER_1_PROJECT_1;
        repository.add(projectExists);

        Assert.assertFalse(repository.existsById(NON_EXISTENT_PROJECT_ID));
        Assert.assertTrue(repository.existsById(projectExists.getId()));
    }

    @Test
    @SneakyThrows
    public void createName() {
        @NotNull final User existentUser = USER_1;

        @NotNull final Project createdProject = repository.create(existentUser.getId(), PROJECT_NAME);
        Assert.assertEquals(PROJECT_NAME, createdProject.getName());
        Assert.assertNotNull(createdProject);
        Assert.assertTrue(repository.existsById(createdProject.getId()));

        @Nullable final Project projectFindOneById = repository.findOneById(createdProject.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdProject.getId(), projectFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void createNameDesc() {
        @NotNull final User existentUser = USER_1;

        @NotNull final Project createdProject = repository.create(existentUser.getId(), PROJECT_NAME, PROJECT_DESC);
        Assert.assertEquals(PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(PROJECT_DESC, createdProject.getDescription());
        Assert.assertNotNull(createdProject);
        Assert.assertTrue(repository.existsById(createdProject.getId()));

        @Nullable final Project projectFindOneById = repository.findOneById(createdProject.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdProject.getId(), projectFindOneById.getId());
    }

}
