package ru.t1.ktubaltseva.tm.repository;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.*;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.service.*;

import java.sql.Connection;
import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.TaskRepTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {

    @NotNull
    private static final ILoggerService loggerService = new LoggerService();

    @NotNull
    private static final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final Connection connection = connectionService.getConnection();

    @NotNull
    private final ITaskRepository repository = new TaskRepository(connection);

    @NotNull
    private final IProjectRepository repositoryProject = new ProjectRepository(connection);

    @NotNull
    private static final IUserService userService = new UserService(propertyService, connectionService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @BeforeClass
    @SneakyThrows
    public static void before() {
        connection.setAutoCommit(true);
        userService.add(USER_1);
        userService.add(USER_2);
        projectService.add(USER_1_PROJECT_1);
        projectService.add(USER_1_PROJECT_2);
    }

    @AfterClass
    @SneakyThrows
    public static void afterClazz() {
        projectService.clear(USER_1.getId());
        projectService.clear(USER_2.getId());
        userService.removeOne(USER_1);
        userService.removeOne(USER_2);
        connection.close();
    }

    @After
    @SneakyThrows
    public void after() {
        repository.removeAll(TASK_LIST);
        repository.clear(USER_1.getId());
        repository.clear(USER_2.getId());
    }

    @Test
    @SneakyThrows
    public void add() {
        @Nullable final Task taskToAdd = USER_1_TASK_1;
        @Nullable final String taskToAddId = taskToAdd.getId();

        @Nullable final Task taskAdded = repository.add((taskToAdd));
        Assert.assertNotNull(taskAdded);
        Assert.assertEquals(taskToAdd.getId(), taskAdded.getId());

        @Nullable final Task taskFindOneById = repository.findOneById(taskToAddId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskToAdd.getId(), taskFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void addByUserId() {
        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final Task taskToAddByUser = USER_1_TASK_1;
        @Nullable final String taskToAddByUserId = taskToAddByUser.getId();

        @Nullable final Task taskAddedByUser = repository.add(userToAddId, taskToAddByUser);
        Assert.assertNotNull(taskAddedByUser);
        Assert.assertTrue(repository.existsById(taskToAddByUserId));

        @Nullable final Task taskFindOneById = repository.findOneById(taskToAddByUserId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskAddedByUser.getId(), taskFindOneById.getId());

        @Nullable final Task taskFindOneByIdByUserIdToAdd = repository.findOneById(userToAddId, taskToAddByUserId);
        Assert.assertNotNull(taskFindOneByIdByUserIdToAdd);
        Assert.assertEquals(taskAddedByUser.getId(), taskFindOneByIdByUserIdToAdd.getId());

        @Nullable final Task taskFindOneByIdByUserIdNoAdd = repository.findOneById(userNoAddId, taskToAddByUserId);
        Assert.assertNull(taskFindOneByIdByUserIdNoAdd);
    }

    @Test
    @SneakyThrows
    public void addMany() {
        @Nullable final Collection<Task> taskList = repository.add(TASK_LIST);
        Assert.assertNotNull(taskList);
        for (@NotNull final Task task : TASK_LIST) {
            @Nullable final Task taskFindOneById = repository.findOneById(task.getId());
            Assert.assertEquals(task.getId(), taskFindOneById.getId());
        }
    }

    @Test
    @SneakyThrows
    public void findOneById() {
        @NotNull final Task taskExists = USER_1_TASK_1;
        repository.add(taskExists);

        @Nullable final Task taskFindOneById = repository.findOneById(taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());

        @Nullable final Task taskFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_TASK_ID);
        Assert.assertNull(taskFindOneByIdNonExistent);
    }

    @Test
    @SneakyThrows
    public void findOneByIdByUserId() {
        @NotNull final Task taskExists = USER_1_TASK_1;
        @NotNull final User userExists = USER_1;
        repository.add(userExists.getId(), taskExists);

        Assert.assertNull(repository.findOneById(userExists.getId(), NON_EXISTENT_TASK_ID));
        Assert.assertNull(repository.findOneById(NON_EXISTENT_USER_ID, taskExists.getId()));

        @Nullable final Task taskFindOneById = repository.findOneById(userExists.getId(), taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists.getId(), taskFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void findAll() {
        @NotNull final Task taskExists = USER_1_TASK_1;

        repository.add(taskExists);
        @NotNull final Collection<Task> tasksFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(tasksFindAllNoEmpty);
    }

    @Test
    @SneakyThrows
    public void findAllByUserId() {
        @NotNull final Task taskExists = USER_1_TASK_1;
        @NotNull final User userExists = USER_1;

        repository.add(userExists.getId(), taskExists);
        @NotNull final Collection<Task> tasksFindAllByUserRepNoEmpty = repository.findAll(userExists.getId());
        Assert.assertNotNull(tasksFindAllByUserRepNoEmpty);

        @NotNull final Collection<Task> tasksFindAllByNonExistentUser = repository.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);
    }

    @Test
    @SneakyThrows
    public void findAllByProjectId() {
        @NotNull final Task taskExists = USER_1_TASK_1;
        @NotNull final User userExists = USER_1;
        @NotNull final Project projectExists = USER_1_PROJECT_1;

        repository.add(userExists.getId(), taskExists);
        @NotNull final Collection<Task> tasksFindAllByUserEmptyProject = repository.findAllByProjectId(userExists.getId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserEmptyProject);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByUserEmptyProject);

        taskExists.setProjectId(projectExists.getId());
        @NotNull final Collection<Task> tasksFindAllByUserBindProject = repository.findAllByProjectId(userExists.getId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserBindProject);

        @NotNull final Collection<Task> tasksFindAllByNonExistentUser = repository.findAllByProjectId(NON_EXISTENT_USER_ID, projectExists.getId());
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);

        @NotNull final Collection<Task> tasksFindAllByNonExistentProject = repository.findAllByProjectId(userExists.getId(), NON_EXISTENT_PROJECT_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentProject);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentProject);
    }

    @Test
    @SneakyThrows
    public void clearByUserId() {
        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<Task> taskByUserToClearList = repository.add(USER_1_TASK_LIST);
        @NotNull final Collection<Task> taskByUserNoClearList = repository.add(USER_2_TASK_LIST);
        repository.clear(userToClearId);

        for (@NotNull final Task taskByUserToClear : taskByUserToClearList) {
            @Nullable final Task taskFindOneById = repository.findOneById(taskByUserToClear.getId());
            Assert.assertNull(taskFindOneById);
        }
        Assert.assertEquals(0, repository.findAll(userToClearId).size());

        for (@NotNull final Task taskByUserNoClear : taskByUserNoClearList) {
            @Nullable final Task taskFindOneById = repository.findOneById(taskByUserNoClear.getId());
            Assert.assertEquals(taskByUserNoClear.getId(), taskFindOneById.getId());
        }
        Assert.assertNotEquals(0, repository.findAll(userNoClearId).size());
    }

    @Test
    @SneakyThrows
    public void removeOne() {
        @Nullable final Task taskToRemove = USER_1_TASK_1;
        repository.add((taskToRemove));

        @Nullable final Task taskRemoved = repository.removeOne(taskToRemove);
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskToRemove.getId(), taskRemoved.getId());

        @Nullable final Task taskFindOneById = repository.findOneById(taskRemoved.getId());
        Assert.assertNull(taskFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeOneByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Task taskByUserToRemove = repository.add((USER_1_TASK_1));
        @Nullable final Task taskByUserNoRemove = repository.add((USER_2_TASK_1));

        Assert.assertNull(repository.removeOne(NON_EXISTENT_TASK_ID, taskByUserToRemove));
        Assert.assertNull(repository.removeOne(userToRemoveId, NON_EXISTENT_TASK));

        @Nullable final Task taskRemoved = repository.removeOne(userToRemoveId, taskByUserToRemove);
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskRemoved.getId(), taskByUserToRemove.getId());
        @Nullable final Task taskRemovedFindOneById = repository.findOneById(taskByUserToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        @Nullable final Task taskNoRemoved = repository.removeOne(userToRemoveId, taskByUserNoRemove);
        Assert.assertNull(taskNoRemoved);
        @Nullable final Task taskNoRemovedFindOneById = repository.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByUserNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void removeById() {
        @Nullable final Task taskToRemove = USER_1_TASK_1;
        repository.add((taskToRemove));

        Assert.assertNull(repository.removeById(NON_EXISTENT_TASK_ID));

        @Nullable final Task taskRemoved = repository.removeById(taskToRemove.getId());
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskToRemove.getId(), taskRemoved.getId());

        @Nullable final Task taskFindOneById = repository.findOneById(taskRemoved.getId());
        Assert.assertNull(taskFindOneById);
    }

    @Test
    @SneakyThrows
    public void removeByIdByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Task taskByUserToRemove = repository.add((USER_1_TASK_1));
        @Nullable final Task taskByUserNoRemove = repository.add((USER_2_TASK_1));

        Assert.assertNull(repository.removeById(NON_EXISTENT_USER_ID, taskByUserToRemove.getId()));
        Assert.assertNull(repository.removeById(userToRemoveId, NON_EXISTENT_TASK_ID));

        @Nullable final Task taskRemoved = repository.removeById(userToRemoveId, taskByUserToRemove.getId());
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskRemoved.getId(), taskByUserToRemove.getId());
        @Nullable final Task taskRemovedFindOneById = repository.findOneById(taskByUserToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        @Nullable final Task taskNoRemoved = repository.removeById(userToRemoveId, taskByUserNoRemove.getId());
        Assert.assertNull(taskNoRemoved);
        @Nullable final Task taskNoRemovedFindOneById = repository.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByUserNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void removeAllByIdByProjectId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final Project projectToRemove = USER_1_PROJECT_1;
        @NotNull final Project projectNoRemove = USER_1_PROJECT_2;
        @Nullable final Task taskByProjectToRemove = USER_1_TASK_1;
        @Nullable final Task taskByProjectNoRemove = USER_1_TASK_2;

        taskByProjectToRemove.setProjectId(projectToRemove.getId());
        taskByProjectNoRemove.setProjectId(projectNoRemove.getId());
        repository.add(taskByProjectToRemove);
        repository.add(taskByProjectNoRemove);
        repository.removeAllByProjectId(userToRemove.getId(), projectToRemove.getId());

        @Nullable final Task taskRemovedFindOneById = repository.findOneById(taskByProjectToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        @Nullable final Task taskNoRemovedFindOneById = repository.findOneById(taskByProjectNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById.getId(), taskByProjectNoRemove.getId());
    }

    @Test
    @SneakyThrows
    public void isExists() {
        @NotNull final Task taskExists = USER_1_TASK_1;
        repository.add(taskExists);

        Assert.assertFalse(repository.existsById(NON_EXISTENT_TASK_ID));
        Assert.assertTrue(repository.existsById(taskExists.getId()));
    }

    @Test
    @SneakyThrows
    public void createName() {
        @NotNull final User existentUser = USER_1;

        @NotNull final Task createdTask = repository.create(existentUser.getId(), TASK_NAME);
        Assert.assertEquals(TASK_NAME, createdTask.getName());
        Assert.assertNotNull(createdTask);
        Assert.assertTrue(repository.existsById(createdTask.getId()));

        @Nullable final Task taskFindOneById = repository.findOneById(createdTask.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(createdTask.getId(), taskFindOneById.getId());
    }

    @Test
    @SneakyThrows
    public void createNameDesc() {
        @NotNull final User existentUser = USER_1;

        @NotNull final Task createdTask = repository.create(existentUser.getId(), TASK_NAME, TASK_DESC);
        Assert.assertEquals(TASK_NAME, createdTask.getName());
        Assert.assertEquals(TASK_DESC, createdTask.getDescription());
        Assert.assertNotNull(createdTask);
        Assert.assertTrue(repository.existsById(createdTask.getId()));

        @Nullable final Task taskFindOneById = repository.findOneById(createdTask.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(createdTask.getId(), taskFindOneById.getId());
    }

}
