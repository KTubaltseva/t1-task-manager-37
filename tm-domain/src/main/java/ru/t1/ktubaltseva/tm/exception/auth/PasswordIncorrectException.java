package ru.t1.ktubaltseva.tm.exception.auth;

public class PasswordIncorrectException extends AbstractAuthException {

    public PasswordIncorrectException() {
        super("Error! Password is incorrect...");
    }

}