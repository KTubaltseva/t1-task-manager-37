package ru.t1.ktubaltseva.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.ktubaltseva.tm.dto.response.AbstractResponse;

@Getter
@Setter
@NoArgsConstructor
public class TaskClearResponse extends AbstractResponse {
}
