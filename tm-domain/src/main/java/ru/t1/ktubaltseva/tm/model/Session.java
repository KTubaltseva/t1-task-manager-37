package ru.t1.ktubaltseva.tm.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.model.IHasCreated;
import ru.t1.ktubaltseva.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public class Session extends AbstractUserOwnedModel implements IHasCreated {

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm a z")
    private Date created = new Date();

    @NotNull
    private Role role = Role.USUAL;

    public Session(@NotNull final User user) {
        this.setUserId(user.getId());
    }

}
