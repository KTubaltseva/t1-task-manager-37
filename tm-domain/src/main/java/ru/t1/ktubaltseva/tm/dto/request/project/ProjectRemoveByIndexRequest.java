package ru.t1.ktubaltseva.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectRemoveByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    public ProjectRemoveByIndexRequest(@Nullable final String token) {
        super(token);
    }

    public ProjectRemoveByIndexRequest(@Nullable final String token, @Nullable final Integer index) {
        super(token);
        this.index = index;
    }

}
