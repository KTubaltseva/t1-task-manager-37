package ru.t1.ktubaltseva.tm.dto.response.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.response.AbstractResponse;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskDisplayListResponse extends AbstractResponse {

    @Nullable
    private List<Task> tasks;

}
