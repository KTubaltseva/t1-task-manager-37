package ru.t1.ktubaltseva.tm.dto.request.task;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;
import ru.t1.ktubaltseva.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public TaskChangeStatusByIndexRequest(@Nullable final String token) {
        super(token);
    }

    public TaskChangeStatusByIndexRequest(@Nullable final String token, @Nullable final Integer index, @Nullable Status status) {
        super(token);
        this.index = index;
        this.status = status;
    }

}
