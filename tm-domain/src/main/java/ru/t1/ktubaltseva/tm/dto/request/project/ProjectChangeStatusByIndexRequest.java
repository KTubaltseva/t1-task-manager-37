package ru.t1.ktubaltseva.tm.dto.request.project;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.dto.request.AbstractUserRequest;
import ru.t1.ktubaltseva.tm.enumerated.Status;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProjectChangeStatusByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private Status status;

    public ProjectChangeStatusByIndexRequest(@Nullable final String token) {
        super(token);
    }

    public ProjectChangeStatusByIndexRequest(@Nullable final String token, @Nullable final Integer index, @Nullable Status status) {
        super(token);
        this.index = index;
        this.status = status;
    }

}