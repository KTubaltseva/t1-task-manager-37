package ru.t1.ktubaltseva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.model.User;

@Getter
@Setter
@NoArgsConstructor
public class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@NotNull final User user) {
        super(user);
    }

}
